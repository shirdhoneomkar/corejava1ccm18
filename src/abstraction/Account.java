package abstraction;

public interface Account {
    void diposit(double amt);
    void withdrow(double amt);
    void checkBal();
}
class SavingAccount implements Account{
    double accountBalance;
    SavingAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("Saving Account created successfully");
    }

    @Override
    public void diposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"rs amount credited in your account");
    }

    @Override
    public void withdrow(double amt) {
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"rs amount debited on your account");
        }
    }

    @Override
    public void checkBal() {
        System.out.println("Saving amount"+accountBalance);
    }
}